﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{
		[SQLite.PrimaryKey, SQLite.AutoIncrement]
		public int id { get; set; }

		public string url { get; set; }
		public string image { get; set; }
		public string title { get; set; }
	}

	public partial class ASSN3Page : ContentPage
	{
		private SQLiteAsyncConnection _connection;
		private List<WebURL> urls;

		public ASSN3Page()
		{
			InitializeComponent();

			_connection = DependencyService.Get<ISQLiteDb>().GetConnection();
		}

		async void ReloadPicker()
		{
			await _connection.CreateTableAsync<WebURL>();
			urls = await _connection.Table<WebURL>().ToListAsync();

			if (ShowURlS.Items.Count != 0)
			{
				ShowURlS.Items.Clear();
			}

			foreach (var x in urls)
			{
				ShowURlS.Items.Add($"{x.title}");
			}
		}

		protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
			var passOnUrl = urls[ShowURlS.SelectedIndex].url;

			await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
		}

		async void AddURLAction(object sender, System.EventArgs e)
		{
			await Navigation.PushModalAsync(new AddURLPage());
		}

		async void UpdateURLAction(object sender, System.EventArgs e)
		{
			urls = await _connection.Table<WebURL>().ToListAsync();
			var selectedItems = ShowURlS.SelectedIndex;
			var selectedClass = urls[ShowURlS.SelectedIndex];


			await Navigation.PushModalAsync(new EditURLPage(selectedClass, selectedItems));
		}

		async void REMOVEURL(object sender, System.EventArgs e)
		{
			urls = await _connection.Table<WebURL>().ToListAsync();

			var selectedItems = urls[ShowURlS.SelectedIndex];
			await _connection.DeleteAsync(selectedItems);

			ReloadPicker();
		}
	}
}